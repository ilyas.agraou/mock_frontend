import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter  } from '@angular/core';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  open: boolean;
  userForm : FormGroup;
  @Output() modalUser = new EventEmitter();

  constructor() { 
    this.userForm = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null,[Validators.required, Validators.minLength(4), Validators.maxLength(10)]) 
  })

 }


  ngOnInit(): void {
  }

  createUser() {
    this.modalUser.emit(this.userForm.value);
    this.userForm.reset();
  }
}

