import { User } from './../models/user';
import { environment } from './../../environments/environment';


import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http : HttpClient) { }

  getAll(){

    return this.http.get(`${environment.apiUrl}/users`)
  }

  Save(data: User) {
    return this.http.post(`${environment.apiUrl}/users`, data);
  }

  delete(userId){
    return this.http.delete(`${environment.apiUrl}/users/`+ userId);
  }

  updateUser(user : any, userId : string){
    return this.http.put(`${environment.apiUrl}/users/`+ userId, user);
  }
  


}
