import { IndexComponent } from './components/index/index.component';
import { PageNotFoundComponent } from './components/partials/page-not-found/page-not-found.component';
import { LoginComponent } from './components/login/login.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {path:"", redirectTo : '/index', pathMatch : 'full'},     // canActivate : [AuthGuard]
  {path : "users", children:[
    {path:"", component : ListUsersComponent},
    {path : "create", component : AddUserComponent},
    {path:"edit/:id", component : EditUserComponent},
  ], //canActivate : [AuthGuard]
  },
  {path : "index", component : IndexComponent},
  {path: "login", component : LoginComponent},
  {path : "**", component : PageNotFoundComponent}
  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
