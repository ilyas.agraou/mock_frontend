import { AccountService } from './../../services/account.service';
import { TokenService } from './../../services/token.service';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email : new FormControl(null, [Validators.required, Validators.email]), //on l'initialise par null
    password : new FormControl(null,[Validators.required, Validators.minLength(4), Validators.maxLength(10)]) //on a fait les validators dans un tableau[] psk on a plusieurs 
  })

  constructor(private authService : AuthService,
      private tokenService : TokenService,
      private accountService : AccountService,
      private router : Router,
      private toastr: ToastrService) { }

  ngOnInit(): void {}
  
  
  login(){
    this.authService.login(this.loginForm.value).subscribe(
      res => this.handleResponse(res),
      err => this.toastr.error(
          `Erreur`,
          'Merci de Vérifier votre email ou mot de passe !',
          {
            timeOut: 3000,
            positionClass: 'toast-bottom-left'
          }
        ))
        }
  
  
/*
  handleResponse(res){
    this.tokenService.handle(res)
    this.router.navigateByUrl("/index")
    this.accountService.changeStatus(true);
  }
  */

  handleResponse(data) {
    this.tokenService.handle(data);
    this.accountService.changeStatus(true);
    this.toastr.success(
      `Bienvenu : ${ this.tokenService.getInfos().name }`,
      'Vous êtes connectés !',
      {
        timeOut: 3000,
        positionClass: 'toast-bottom-left'
      }
    );
    this.router.navigateByUrl('/');
  }
  
  




}
