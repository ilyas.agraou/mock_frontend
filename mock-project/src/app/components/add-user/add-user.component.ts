import { UsersService } from './../../services/users.service';
import { User } from './../../models/user';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  users : User[];

  constructor(private userService : UsersService) { }

  ngOnInit(): void {
    this.userService.getAll().subscribe((res: User[]) => this.users = res)
  }

  persistUser(data: User) {
    this.userService.Save(data)
        .subscribe((res: User) => this.users = [res, ...this.users])
  }

  

}
