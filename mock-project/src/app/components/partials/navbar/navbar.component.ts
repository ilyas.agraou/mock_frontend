
import { Router } from '@angular/router';
import { TokenService } from './../../../services/token.service';
import { AccountService } from './../../../services/account.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  currentUser : boolean = false;
  userInfos : any = null;
  constructor(private accountService : AccountService, private toastr: ToastrService, private tokenService : TokenService, private router : Router) { }

  ngOnInit(): void {
    this.accountService.authStatus.subscribe(res =>{
      this.currentUser = res
      this.userInfos = this.tokenService.getInfos();
  })
  }
  logout(){
    this.tokenService.remove();
    this.accountService.changeStatus(false);
    this.toastr.info(
      'Déconnexion',
      'Vous êtes déconnecter !',
      {
        timeOut: 3000,
        positionClass: 'toast-bottom-left'
      }
  )
    this.router.navigateByUrl("/login");
  }
  

}
