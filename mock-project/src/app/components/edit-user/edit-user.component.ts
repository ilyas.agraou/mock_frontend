import { User } from './../../models/user';
import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  @Input() user : User = null;

  @Output() editUser = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
  }

  edit() {

    this.editUser.emit(this.user);
  }

}
