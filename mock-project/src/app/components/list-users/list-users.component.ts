import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from './../../models/user';
import { UsersService } from './../../services/users.service';
import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  userForm: FormGroup;
  @Output() modalUser = new EventEmitter();
  open: boolean;
  @Input() user: User = null;

  userToUpdate = {
    userId : "",
    firstName : "",
    lastName : "",
    email : "",
    password : ""
  };

  constructor(private usersService : UsersService, private router : Router, @Inject(DOCUMENT) private _document: Document) {
    this.userForm = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required) 
  }),
  this.getAllUsers();
   }

  users : User[] = [];


  ngOnInit(): void {
    this.getAllUsers();
  }

  refreshPage() {
    this._document.defaultView.location.reload();
  }

  getAllUsers(){
    this.usersService.getAll().subscribe((res : User[]) => this.users = res );
  }

  persistUser(data: User) {
    this.usersService.Save(data)
        .subscribe((res: User) => this.users = [res, ...this.users])
  }

  deleteUser(user){
    this.usersService.delete(user.userId).subscribe(
      data=>{
      console.log(data);
      
    });
    this.refreshPage();
  }
  
 edit(user){
   this.userToUpdate = user;
 }
/*
 edits(){
   this.modalUser.emit(this.user);
 }
 */
 
  updateUser(){
    this.usersService.updateUser(this.userToUpdate, this.userToUpdate.userId).subscribe(
      (resp) => {
        console.log(resp);
      },
      (err) => {
        console.log(err);
      }
    );
    this.refreshPage();
  }
  

  

}
