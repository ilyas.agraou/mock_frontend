import { JwtInterceptor } from './services/jwt.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { DeleteUserComponent } from './components/delete-user/delete-user.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { LoginComponent } from './components/login/login.component';
import { PartialsComponent } from './components/partials/partials.component';
import { PageNotFoundComponent } from './components/partials/page-not-found/page-not-found.component';
import { NavbarComponent } from './components/partials/navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IndexComponent } from './components/index/index.component';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './modal/modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';  


@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    EditUserComponent,
    DeleteUserComponent,
    ListUsersComponent,
    LoginComponent,
    PartialsComponent,
    PageNotFoundComponent,
    NavbarComponent,
    IndexComponent,
    ModalComponent
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    NgbModule,
    FormsModule

  ],
  providers: [{
    provide : HTTP_INTERCEPTORS,
    useClass : JwtInterceptor,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
